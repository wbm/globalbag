package videotms

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

//////
// Vars, and types.
//////

// Re-usable, private, cached video TMS client.
var tmsClient *Client

// Client definition.
type Client struct {
	BaseEndpoint string
	Bearer       string

	// Immutable authentication information.
	auth ClientAuth

	// Cached/re-usable HTTP client.
	client http.Client

	// Time when authentication (token) expires.
	expiresIn time.Time
}

// OAuth's client information.
type ClientAuth struct {
	ClientID     string
	ClientSecret string
}

// OAuth's request information.
type ClientAuthReq struct {
	Audience     string `json:"audience"`
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
	GrantType    string `json:"grant_type"`
}

// OAuth request's response.
// NOTE: Modern flow adds `refresh_token`.
type ClientAuthResp struct {
	AccessToken string `json:"access_token"`

	// SEE: https://stackoverflow.com/a/30826806.
	expiresIn int `json:"expires_in"` // In seconds.
}

//////
// Helpers.
//////

func AlphaNumericOnly(str string) string {
	var result bytes.Buffer

	for _, r := range str {
		if (r >= 'a' && r <= 'z') || (r >= 'A' && r <= 'Z') || (r >= '0' && r <= '9') || (r == ' ') {
			result.WriteRune(r)
		}
	}

	return result.String()
}

//////
// Entrypoint.
//////

// Setup client.
func SetupClient(auth ClientAuth, baseEndpoint string) {
	log.Println("Setting up VideoTMS client - ", baseEndpoint)

	tmsClient = &Client{
		BaseEndpoint: baseEndpoint,

		// NOTE: Client's Transport typically has internal state (cached TCP  conn),
		// so clients should be REUSED instead of created as needed. Clients are
		// safe for concurrent use by multiple goroutines.
		client: http.Client{},
		auth:   auth,
	}

	tmsClient.authenticate()

}

// Get returns a setup client.
//
// NOTE: It's an idempotent function - if called many times, the output will be
// always the same. The client is essential, if it isn't setup, it'll exit the
// application.
func Get() *Client {
	if tmsClient != nil {
		return tmsClient
	}

	log.Fatal("Can't return TMS video client, it wasn't setup")

	return nil
}

//////
// Internal methods.
//////

// Authenticates client.
// TODO: Should use smth like this: https://github.com/golang/oauth2.
func (c *Client) authenticate() error {

	log.Println("VideoTMS - Authenticating")

	// Should do nothing if token isn't expired.
	if c.expiresIn.After(time.Now()) {
		log.Println("VideoTMS - Reusing active credentials")
		return nil
	}

	authReqJson, _ := json.Marshal(ClientAuthReq{
		Audience:     "https://videotms.com/api/v3/",
		ClientID:     c.auth.ClientID,
		ClientSecret: c.auth.ClientSecret,
		GrantType:    "client_credentials",
	})
	authReqBuff := bytes.NewBuffer(authReqJson)
	req, err := http.NewRequest(http.MethodPost, "https://login.videotms.com/oauth/token", authReqBuff)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")

	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	var authResponse ClientAuthResp
	if err := json.Unmarshal(bodyData, &authResponse); err != nil {
		return err
	}

	c.Bearer = authResponse.AccessToken

	// Should expire in 24h if no expiration is set.
	if authResponse.expiresIn == 0 {
		authResponse.expiresIn = 86400
	}

	c.expiresIn = time.Now().Add(time.Duration(authResponse.expiresIn) * time.Second)

	log.Println("VideoTMS - Authentication complete")

	return nil
}

// Executes a request. It'll automatically authenticate. If the token is expired
// it'll automatically retrieve a new one.
func (c *Client) doRequest(req *http.Request) (*http.Response, error) {
	c.authenticate()

	return c.client.Do(req)
}

//////
// Exported functionalities.
//////

func (c *Client) CreateMedia(params MediaCreateParams) (MediaCreateResp, error) {
	// Remove non-alpha numeric characters from the media title
	params.MediaDTO.Title = AlphaNumericOnly(params.MediaDTO.Title)
	params.MediaDTO.Description = AlphaNumericOnly(params.MediaDTO.Description)

	var response MediaCreateResp

	authReqJson, marshalErr := json.Marshal(params)
	if marshalErr != nil {
		return response, marshalErr
	}

	authReqBuff := bytes.NewBuffer(authReqJson)

	req, reqErr := http.NewRequest(http.MethodPost, fmt.Sprintf("%v/media", c.BaseEndpoint), authReqBuff)
	if reqErr != nil {
		return response, reqErr
	}

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", c.Bearer))
	req.Header.Add("Content-Type", "application/json")

	resp, err := c.doRequest(req)
	if err != nil {
		return MediaCreateResp{}, err
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return MediaCreateResp{}, err
	}

	// Print response status code
	log.Println("VideoTMS - Response status code: ", resp.StatusCode)

	err = json.Unmarshal(respBody, &response)
	if err != nil {
		return MediaCreateResp{}, err
	}

	if resp.StatusCode != http.StatusOK {
		fmt.Printf("VideoTMS - Error occurred when attempting to create media. Status code: %d\n Response Body: %v", resp.StatusCode, string(respBody))
		return MediaCreateResp{}, errors.New(response.ErrorMessage)
	}

	return response, nil
}

func (c *Client) DownloadCaptions(mediaID, languageID, subtitleFormat string) (string, error) {
	log.Println("VideoTMS - Preparing caption download request for ", mediaID, languageID)

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%v/media/%v/languages/%v/subtitles/%v", c.BaseEndpoint, mediaID, languageID, subtitleFormat), nil)

	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", c.Bearer))
	if err != nil {
		return "", err
	}

	resp, err := c.doRequest(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return "", errors.New(fmt.Sprintf("VideoTMS - Error occurred when attempting to download caption content. Status code: %d", resp.StatusCode))
	}

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	log.Println("VideoTMS - Caption download completed")

	return string(bodyData), nil
}
