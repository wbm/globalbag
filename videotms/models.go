package videotms

import "time"

type Response struct {
	Status ResponseStatus `json:"status"`
}
type ResponseStatus struct {
	Code    int    `json:"code"`
	Error   bool   `json:"error"`
	Message string `json:"message"`
	UUID    string `json:"uuid"`
}

type ListResponse struct {
	Offset        int         `json:"offset"`
	Pagesize      int         `json:"pagesize"`
	Result        interface{} `json:"result"`
	SortDirection string      `json:"sortDirection"`
	SortField     string      `json:"sortField"`
	TotalPages    int         `json:"totalPages"`
	TotalResults  int         `json:"totalResults"`
}

type Media struct {
	DateCreated int `json:"dateCreated"`
	// A description of this video. This can not be more than 800 characters.
	Description string `json:"description"`
	//The director of this video.
	Director string `json:"director"`
	// The URI of the page that displays this video on Dotsub.
	DisplayURI string `json:"displayURI"`
	// The runtime of the video in milliseconds.
	Duration int `json:"duration"`
	// The external ID of this video.
	ExternalIdentifier string `json:"externalIdentifier"`
	Genre              string `json:"genre"`
	IsPublic           bool   `json:"isPublic"`
	// The source language of the video. This is the 3 letter ISO code that can be found using the languages API call.
	Language string `json:"language"`
	// Information on the languages available for a video. See Language type for more detail.
	Languages    map[string]Language `json:"languages"`
	LastModified int                 `json:"lastModified"`
	// The license of this video. Please see 'License Return' for field descriptions
	License struct {
		Description string `json:"description"`
		ID          string `json:"id"`
		Name        string `json:"name"`
		URL         string `json:"url"`
	} `json:"license"`
	// The number languages this video is in.
	NumberOfLanguages int `json:"numberOfLanguages"`
	// The number translations this video has.
	NumberOfTranslations int `json:"numberOfTranslations"`
	// The number of view this video has.
	NumberOfViews int `json:"numberOfViews"`
	// The producer of this video.
	Producer string `json:"producer"`
	// A URI that returns the videos thumbmail preview.
	ScreenshotURI string `json:"screenshotURI"`
	// The title of this video.
	Title string `json:"title"`
	// The estimated completion of a videos transcription.
	TranscriptionCompletion int `json:"transcriptionCompletion"`
	// The current status of a video. This can be NEW, COMPLETED.
	TranscriptionStatus string `json:"transcriptionStatus"`
	// The URI for translation interface. Appending the language code at the end of this will load a translation interface
	TranslateURIPattern string `json:"translateURIPattern"`
	// The type of video. This can be LocalMedia which are files uploaded to Dotsub or YouTubeMedia which are videos stored on YouTube but subtitled on Dotsub.
	Type string `json:"type"`
	// The username of the user that posted this video.
	User string `json:"user"`
	// The Dotsub unique identifier (ID) of this video.
	UUID string `json:"uuid"`
	// The current status of a video. This can be NEW, PROCESSING, PROCESSING_FAILED or READY.
	WorkflowStatus string `json:"workflowStatus"`
}

type Language struct {
	// The list of people working on this language. This returns a map of usernames.
	Actors      map[string]string `json:"actors"`
	Description string            `json:"description"`
	// The Dotsub language code of this language.
	LanguageCode string `json:"languageCode"`
	// The writing direction of the language. This can be 'rtl' or 'ltr'. 'rtl' denotes language written right to left, while 'ltr' denotes a language written left to right.
	LanguageDirection string `json:"languageDirection"`
	// The name of the language localized in English.
	LanguageName string `json:"languageName"`
	// The name of the language in it's native tongue. (ex: Español, Français)
	LanguageNameLocalized string `json:"languageNameLocalized"`
	// The quality level of a given item. This is only available to enterprise clients.
	Level string `json:"level"`
	// How complete a given translation is.
	PercentageComplete int      `json:"percentageComplete"`
	Properties         struct{} `json:"properties"` // No longer used
	// The title of this video in this language.
	Title string `json:"title"`
	// The work status of a language. This can be one of: NONE, ASSIGNED, TRANSLATED, TRANSCRIBED, REVISED, PUBLISHED. This is only available to enterprise clients.
	WorkflowStatus             string `json:"workflowStatus"`
	WorkflowStatusLastModified int    `json:"workflowStatusLastModified"`
	DateCreated                int    `json:"dateCreated"`
	LastModified               int    `json:"lastModified"`
}

type MediaCreateParams struct {
	S3Upload    bool     `json:"s3Upload,omitempty"`
	ImportURL   string   `json:"importUrl,omitempty"`
	ContentType string   `json:"contentType,omitempty"`
	MediaDTO    MediaDTO `json:"mediaDto"`
}

type MediaCreateResp struct {
	MediaID                  string      `json:"mediaId"`
	Title                    string      `json:"title"`
	ProjectID                string      `json:"projectId"`
	ProjectName              string      `json:"projectName"`
	WorkflowName             string      `json:"workflowName"`
	ClientID                 string      `json:"clientId"`
	ClientName               string      `json:"clientName"`
	Description              string      `json:"description"`
	Webhook                  string      `json:"webhook"`
	Notes                    string      `json:"notes"`
	ErrorMessage             string      `json:"errorMessage"`
	MediaState               string      `json:"mediaState"`
	CreatedDate              time.Time   `json:"createdDate"`
	WorkflowID               string      `json:"workflowId"`
	WorkflowTemplateOutdated interface{} `json:"workflowTemplateOutdated"`
	OrganizationID           string      `json:"organizationId"`
	OrganizationName         string      `json:"organizationName"`
	ExternalID               string      `json:"externalId"`
	FileName                 string      `json:"fileName"`
	Language                 struct {
		ID          string `json:"id"`
		Code        string `json:"code"`
		CountryCode string `json:"countryCode"`
		Alpha2BCode string `json:"alpha2BCode"`
		Alpha2TCode string `json:"alpha2TCode"`
		Alpha1Code  string `json:"alpha1Code"`
		Name        string `json:"name"`
		Direction   string `json:"direction"`
	} `json:"language"`
	ReviewLanguages []struct {
		ID          string `json:"id"`
		Code        string `json:"code"`
		CountryCode string `json:"countryCode"`
		Alpha2BCode string `json:"alpha2BCode"`
		Alpha2TCode string `json:"alpha2TCode"`
		Alpha1Code  string `json:"alpha1Code"`
		Name        string `json:"name"`
		Direction   string `json:"direction"`
	} `json:"reviewLanguages"`
	TranslationLanguages []struct {
		ID          string `json:"id"`
		Code        string `json:"code"`
		CountryCode string `json:"countryCode"`
		Alpha2BCode string `json:"alpha2BCode"`
		Alpha2TCode string `json:"alpha2TCode"`
		Alpha1Code  string `json:"alpha1Code"`
		Name        string `json:"name"`
		Direction   string `json:"direction"`
	} `json:"translationLanguages"`
	SubtitledLanguageIds interface{} `json:"subtitledLanguageIds"`
	Tags                 []struct {
		TagID        string `json:"tagId"`
		Name         string `json:"name"`
		Color        string `json:"color"`
		TextColor    string `json:"textColor"`
		Workflow     string `json:"workflow"`
		ColorHex     string `json:"colorHex"`
		TextColorHex string `json:"textColorHex"`
	} `json:"tags"`
	UploadDetail struct {
		ID                string      `json:"id"`
		Start             time.Time   `json:"start"`
		Finish            interface{} `json:"finish"`
		Origin            string      `json:"origin"`
		Status            string      `json:"status"`
		Percentage        int         `json:"percentage"`
		ErrorMessage      string      `json:"errorMessage"`
		RawFileStorageURL string      `json:"rawFileStorageUrl"`
		Ovp               interface{} `json:"ovp"`
		UploadUpdateToken string      `json:"uploadUpdateToken"`
	} `json:"uploadDetail"`
	EncodedMedia struct {
		ID                           string `json:"id"`
		Mp4URL                       string `json:"mp4Url"`
		RawS3Mp4URL                  string `json:"rawS3Mp4Url"`
		Mp4HdURL                     string `json:"mp4HdUrl"`
		GifURL                       string `json:"gifUrl"`
		ThumbnailURL                 string `json:"thumbnailUrl"`
		Error                        string `json:"error"`
		OriginalLengthInMilliseconds int32  `json:"originalLengthInMilliseconds"`
		EncodedLengthInMilliseconds  int32  `json:"encodedLengthInMilliseconds"`
		ReprocessWith                string `json:"reprocessWith"`
	} `json:"encodedMedia"`
	Metadata struct {
		Type           string `json:"type"`
		Length         int    `json:"length"`
		Width          int    `json:"width"`
		Height         int    `json:"height"`
		Visible        bool   `json:"visible"`
		UploaderUserID string `json:"uploaderUserId"`
		SignedURL      string `json:"signedUrl"`
	} `json:"metadata"`
	ReplacedImportedURL string      `json:"replacedImportedUrl"`
	ChunkParentMediaID  string      `json:"chunkParentMediaId"`
	ChunkIndex          int         `json:"chunkIndex"`
	ChunkStart          int         `json:"chunkStart"`
	ChunkEnd            int         `json:"chunkEnd"`
	MediaChunkIds       interface{} `json:"mediaChunkIds"`
}

type MediaDTO struct {
	Title       string `json:"title"`
	ProjectID   string `json:"projectId"`
	FileName    string `json:"fileName"`
	Description string `json:"description"`
	Notes       string `json:"notes"`
	Language    struct {
		ID string `json:"id"`
	} `json:"language"`
	TranslationLanguages []struct {
		ID string `json:"id"`
	} `json:"translationLanguages"`
	ReviewLanguages []struct {
		ID string `json:"id"`
	} `json:"reviewLanguages"`
	Tags []struct {
		Name string `json:"name"`
	} `json:"tags"`
	WorkflowID string `json:"workflowId,omitempty"`
	ExternalID string `json:"externalId"`
	Webhook    string `json:"webhook"`
}

type MetadataUpdateParams struct {
	S3Upload    bool     `json:"s3Upload"`
	ImportURL   string   `json:"importUrl"`
	ContentType string   `json:"contentType"`
	MediaDTO    MediaDTO `json:"mediaDto"`
}

type MetadataGetParams struct {
	UUID                     string
	IncludeEmptyTranslations bool
}

type Notification struct {
	DateTime        time.Time `json:"dateTime"`
	Action          string    `json:"action"`
	TaskID          string    `json:"taskId"`
	ProjectID       string    `json:"projectId"`
	MediaID         string    `json:"mediaId"`
	LanguageID      string    `json:"languageId"`
	MediaExternalID string    `json:"mediaExternalId"`
	MediaTags       []string  `json:"mediaTags"`
	ErrorDetail     string    `json:"errorDetail"`
}

type CaptionResponse struct {
	Captions []Caption `json:"captions"`
}

type Caption struct {
	Content          string `json:"content"`
	StartTime        int    `json:"startTime"`
	Duration         int    `json:"duration"`
	StartOfParagraph bool   `json:"startOfParagraph"`
}
