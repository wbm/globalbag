package dotsub

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

const (
	BaseEndpoint = "https://dotsub.com/api"
)

type Client struct {
	Username string
	Password string
}

func NewClient(username, password string) *Client {
	return &Client{
		Username: username,
		Password: password,
	}
}

// CreateMedia - You can upload media to Dotsub from your system using a HTTP Post to our media upload API.
// This form does require you to be authenticated.
// Path: https://dotsub.com/api/media
// Method: POST
func (c *Client) CreateMedia(params MediaCreateParams) (Response, error) {
	var response Response

	var endpoint = fmt.Sprintf(BaseEndpoint + "/media")
	resp, err := c.Request(endpoint, http.MethodPost, params)
	if err != nil {
		return Response{}, err
	}

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Response{}, err
	}

	err = json.Unmarshal(bodyData, &response)
	if err != nil {
		return Response{}, err
	}

	return response, nil
}

// GetMetadata - You can retrieve the metadata of a given media via this call.
// Path: https://dotsub.com/api/media/$UUID or https://dotsub.com/api/user/$username/media/$externalIdentifier
// Method: GET
// Parameters:
/// includeEmptyTranslations: If you set this parameter to true, translations with no work done will also be included in the response. This defaults to false.
func (c *Client) GetMetadata(params MetadataGetParams) (Media, error) {
	var media Media

	endpoint := fmt.Sprintf("%v/media/%v", BaseEndpoint, params.UUID)
	resp, err := c.Request(endpoint, http.MethodGet, nil)
	if err != nil {
		return Media{}, err
	}

	defer resp.Body.Close()

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Media{}, err
	}

	err = json.Unmarshal(bodyData, &media)
	if err != nil {
		return Media{}, err
	}

	return media, nil
}

// GetCaptions - You can retrieve the captions of a given media in a selected language via this call.
// Path: https://dotsub.com/api/media/$UUID/captions?language=$languageCode or https://dotsub.com/api/user/$username/media/$externalIdentifier/captions?language=$languageCode
// Method: GET
func (c *Client) GetCaptions(UUID, languageCode string) (CaptionResponse, error) {
	var response CaptionResponse

	endpoint := fmt.Sprintf("%v/media/%v/captions", BaseEndpoint, UUID)
	resp, err := c.Request(endpoint, http.MethodGet, struct {
		Language string `json:"language"`
	}{
		languageCode,
	})
	if err != nil {
		return CaptionResponse{}, err
	}

	defer resp.Body.Close()

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return CaptionResponse{}, err
	}

	err = json.Unmarshal(bodyData, &response)
	if err != nil {
		return CaptionResponse{}, err
	}

	return response, nil
}

// DownloadCaptions - Translation and transcription files can be downloaded from Dotsub using the following requests. Any private videos will require you to be authenticated to download.
// Path: https://dotsub.com/media/$UUID/c/$languageCode/$format or https://dotsub.com/media/u/$username/$externalIdentifier/c/$languageCode/$format
// Method: POST
func (c *Client) DownloadCaptions(UUID, languageCode, format string) (string, error) {
	endpoint := fmt.Sprintf("https://dotsub.com/media/%v/c/%v/%v", UUID, languageCode, format)
	resp, err := c.Request(endpoint, http.MethodPost, nil)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()

	bodyData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(bodyData), nil
}

func (c *Client) Request(endpoint string, method string, params interface{}) (*http.Response, error) {
	client := http.Client{}
	queryString := url.Values{}

	// Prepare request body
	if params != nil {
		paramJson, err := json.Marshal(params)
		if err != nil {
			return &http.Response{}, err
		}

		var paramMap map[string]interface{}
		if err := json.Unmarshal(paramJson, &paramMap); err != nil {
			return &http.Response{}, err
		}

		for k, v := range paramMap {
			queryString.Add(k, fmt.Sprint(v))
		}
	}

	// Prepare Request
	req, err := http.NewRequest(method, endpoint+"?"+queryString.Encode(), nil)
	if err != nil {
		return &http.Response{}, err
	}
	req.SetBasicAuth(c.Username, c.Password)

	// Perform Request
	return client.Do(req)
}
