package dotsub

type Response struct {
	Status ResponseStatus `json:"status"`
}
type ResponseStatus struct {
	Code    int    `json:"code"`
	Error   bool   `json:"error"`
	Message string `json:"message"`
	UUID    string `json:"uuid"`
}

type ListResponse struct {
	Offset        int         `json:"offset"`
	Pagesize      int         `json:"pagesize"`
	Result        interface{} `json:"result"`
	SortDirection string      `json:"sortDirection"`
	SortField     string      `json:"sortField"`
	TotalPages    int         `json:"totalPages"`
	TotalResults  int         `json:"totalResults"`
}

type Media struct {
	DateCreated int `json:"dateCreated"`
	// A description of this video. This can not be more than 800 characters.
	Description string `json:"description"`
	//The director of this video.
	Director string `json:"director"`
	// The URI of the page that displays this video on Dotsub.
	DisplayURI string `json:"displayURI"`
	// The runtime of the video in milliseconds.
	Duration int `json:"duration"`
	// The external ID of this video.
	ExternalIdentifier string `json:"externalIdentifier"`
	Genre              string `json:"genre"`
	IsPublic           bool   `json:"isPublic"`
	// The source language of the video. This is the 3 letter ISO code that can be found using the languages API call.
	Language string `json:"language"`
	// Information on the languages available for a video. See Language type for more detail.
	Languages    map[string]Language `json:"languages"`
	LastModified int                 `json:"lastModified"`
	// The license of this video. Please see 'License Return' for field descriptions
	License struct {
		Description string `json:"description"`
		ID          string `json:"id"`
		Name        string `json:"name"`
		URL         string `json:"url"`
	} `json:"license"`
	// The number languages this video is in.
	NumberOfLanguages int `json:"numberOfLanguages"`
	// The number translations this video has.
	NumberOfTranslations int `json:"numberOfTranslations"`
	// The number of view this video has.
	NumberOfViews int `json:"numberOfViews"`
	// The producer of this video.
	Producer string `json:"producer"`
	// A URI that returns the videos thumbmail preview.
	ScreenshotURI string `json:"screenshotURI"`
	// The title of this video.
	Title string `json:"title"`
	// The estimated completion of a videos transcription.
	TranscriptionCompletion int `json:"transcriptionCompletion"`
	// The current status of a video. This can be NEW, COMPLETED.
	TranscriptionStatus string `json:"transcriptionStatus"`
	// The URI for translation interface. Appending the language code at the end of this will load a translation interface
	TranslateURIPattern string `json:"translateURIPattern"`
	// The type of video. This can be LocalMedia which are files uploaded to Dotsub or YouTubeMedia which are videos stored on YouTube but subtitled on Dotsub.
	Type string `json:"type"`
	// The username of the user that posted this video.
	User string `json:"user"`
	// The Dotsub unique identifier (ID) of this video.
	UUID string `json:"uuid"`
	// The current status of a video. This can be NEW, PROCESSING, PROCESSING_FAILED or READY.
	WorkflowStatus string `json:"workflowStatus"`
}

type Language struct {
	// The list of people working on this language. This returns a map of usernames.
	Actors      map[string]string `json:"actors"`
	Description string            `json:"description"`
	// The Dotsub language code of this language.
	LanguageCode string `json:"languageCode"`
	// The writing direction of the language. This can be 'rtl' or 'ltr'. 'rtl' denotes language written right to left, while 'ltr' denotes a language written left to right.
	LanguageDirection string `json:"languageDirection"`
	// The name of the language localized in English.
	LanguageName string `json:"languageName"`
	// The name of the language in it's native tongue. (ex: Español, Français)
	LanguageNameLocalized string `json:"languageNameLocalized"`
	// The quality level of a given item. This is only available to enterprise clients.
	Level string `json:"level"`
	// How complete a given translation is.
	PercentageComplete int      `json:"percentageComplete"`
	Properties         struct{} `json:"properties"` // No longer used
	// The title of this video in this language.
	Title string `json:"title"`
	// The work status of a language. This can be one of: NONE, ASSIGNED, TRANSLATED, TRANSCRIBED, REVISED, PUBLISHED. This is only available to enterprise clients.
	WorkflowStatus             string `json:"workflowStatus"`
	WorkflowStatusLastModified int    `json:"workflowStatusLastModified"`
	DateCreated                int    `json:"dateCreated"`
	LastModified               int    `json:"lastModified"`
}

type MediaCreateParams struct {
	// The title of this video.
	Title string `json:"title"`
	// A description of this video. This can not be more than 800 characters.
	Description string `json:"description"`
	// The source language of the video. This is the 3 letter ISO code that can be found using the languages API call.
	Language  string `json:"language"`
	LicenseID string `json:"license"`
	// A url to the file to be uploaded to Dotsub. This must be a url directly to a video file not a webpage that contains a video.
	// REQUIRED IF file PARAM DOES NOT EXIST
	URL string `json:"url"`
	// The file you are uploading. This should be included as part of a multi-part post request.
	// REQUIRED IF url PARAM DOES NOT EXIST
	// Unsupported and unnecessary right now
	// File byte `json:"file"`
	// The producer of this video.
	Producer string `json:"producer,omitempty"`
	//The director of this video.
	Director string `json:"director,omitempty"`
	// This value determines if the video is publicly listed on Dotsub. This value defaults to false.
	Publicity bool `json:"publicity"`
	// This value determines if a video is listed on the public listing pages (Latest, Most Viewed, Videos from User). Defaults to 'true'. This option is only used if publicity is 'true'.
	Public bool `json:"publicallyViewable"`
	// The external ID of this video.
	ExternalIdentifier string `json:"externalIdentifier"`
	// The ID of the project you want to post this video to. Please consult the Project API to get a listing of the projects available to you.
	ProjectID string `json:"project"`
	// This determines if a video will default to the user that is specified on the project. Default 'true' if project is present.
	PostAsDefault bool `json:"postToProjectAsDefaultUser"`
	// If you have a standing caption or translation order this lets us know this item should not be captioned or translated. Defaults to 'false'.
	DoNotCaption bool `json:"doNotCaption"`
}

type MetadataUpdateParams struct {
	// The title of this video.
	Title string `json:"title"`
	// A description of this video. This can not be more than 800 characters.
	Description string `json:"description"`
	// The country code of this video. Ex: CA, US, CZ
	Country string `json:"country"`
	// The source language of the video. This is the 3 letter ISO code that can be found using the languages API call.
	Language string `json:"language"`
	// The producer of this video.
	Producer string `json:"producer"`
	//The director of this video.
	Director string `json:"director"`
	// This value determines if the video is publicly listed on Dotsub. This value defaults to false.
	Publicity bool `json:"publicity"`
	// This value determines if a video is listed on the public listing pages (Latest, Most Viewed, Videos from User). Defaults to 'true'. This option is only used if publicity is 'true'.
	Public bool `json:"publicallyViewable"`
	// The external ID of this video.
	ExternalIdentifier string `json:"externalIdentifier"`
	// The ID of the project you want to post this video to. Please consult the Project API to get a listing of the projects available to you.
	Project string `json:"project"`
}

type MetadataGetParams struct {
	UUID                     string
	IncludeEmptyTranslations bool
}

type Notification struct {
	// The Dotsub unique identifier (ID) of this video.
	UUID string `json:"uuid"`
	// The external ID of this video.
	ExternalIdentifier string `json:"externalIdentifier"`
	// The source language of the video. This is the 3 letter ISO code that can be found using the languages API call.
	Language string `json:"language"`
	// The type of event happening for this translation
	State string `json:"state"`
}

type CaptionResponse struct {
	Captions []Caption `json:"captions"`
}

type Caption struct {
	Content          string `json:"content"`
	StartTime        int    `json:"startTime"`
	Duration         int    `json:"duration"`
	StartOfParagraph bool   `json:"startOfParagraph"`
	TTS              `json:"tts"`
}

// TTS properties needed for AWS Polly TextToSpeech functionality.
//   - ActualDuration - Tracks time it takes polly to enunciate SSML formatted
//     subtitles, including time-duration constraints and breaks. Not to be
//     confused with the actual time it takes Polly to enunciate subtitle without
//     SSML, time constraints or breaks.
//   - AdjustPause - Additional breaks to keep audio and subtitles from drifting.
//   - CatchUP - Tracks amount time audio is ahead of subtitles.
//   - EndTime - Millisecond when the subtitle duration time ends.
//   - LinePause - Pause between subtitles provided in SRT file.
//   - StartPause - Pause at the beginning of the video.
type TTS struct {
	ActualDuration int `json:"actualDuration"`
	AdjustedPause  int `json:"adjustedPause"`
	CatchUp        int `json:"catchUp"`
	EndTime        int `json:"endTime"`
	LinePause      int `json:"linePause"`
	StartPause     int `json:"startPause"`
}
