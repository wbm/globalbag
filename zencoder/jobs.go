package zencoder

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// CreateJobResponse from CreateJob
type CreateJobResponse struct {
	ID      int  `json:"id,omitempty"`
	Test    bool `json:"test,omitempty"`
	Outputs []struct {
		ID    int    `json:"id,omitempty"`
		Label string `json:"label,omitempty"`
		URL   string `json:"url,omitempty"`
	} `json:"outputs,omitempty"`
}

type ErrorResponse struct {
	Errors []string `json:"errors"`
}

// JobSetting ...
// ZenDoc: https://app.zencoder.com/docs/api/jobs/create
type JobSetting struct {
	Input                 string            `json:"input"`
	Region                string            `json:"region,omitempty"`
	Test                  bool              `json:"test,omitempty"`
	Private               bool              `json:"private,omitempty"`
	DownloadConnections   int               `json:"download_connections,omitempty"`
	PassThrough           string            `json:"pass_through,omitempty"`
	Mock                  bool              `json:"mock,omitempty"`
	Grouping              string            `json:"grouping,omitempty"`
	AsperaTransferPolicy  string            `json:"aspera_transfer_policy,omitempty"`
	AsperaTransferMinRate int               `json:"transfer_minimum_rate,omitempty"`
	AsperaTransferMaxRate int               `json:"transfer_maximum_rate,omitempty"`
	ExpectedMD5Checksum   string            `json:"expected_md5_checksum,omitempty"`
	Credentials           string            `json:"credentials,omitempty"`
	Output                []*OutputSettings `json:"output,omitempty"`
}

type JobProgressResponse struct {
	State    string  `json:"state"`
	Progress float64 `json:"progress"`
	Input    struct {
		ID    int    `json:"id"`
		State string `json:"state"`
	} `json:"input"`
	Outputs []struct {
		ID                   int     `json:"id"`
		State                string  `json:"state"`
		CurrentEvent         string  `json:"current_event"`
		CurrentEventProgress float64 `json:"current_event_progress"`
		Progress             float64 `json:"progress"`
	} `json:"outputs"`
}

func (z *Client) GetJobProgress(id string) (*JobProgressResponse, error) {

	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	client := &http.Client{}

	result := JobProgressResponse{}

	req, err := z.request(GET, fmt.Sprintf("jobs/%v/progress", id), nil)
	if err != nil {
		return &result, err
	}

	resp, err := client.Do(req.WithContext(ctx))
	if err != nil {
		return &result, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &result, err
	}

	if err := json.Unmarshal(body, &result); err != nil {
		return &result, err
	}

	return &result, nil

}

// CreateJob creates a zencoder job via POST response.
func (z *Client) CreateJob(setting JobSetting) (*CreateJobResponse, error) {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, 5*time.Second)
	defer cancel()
	client := &http.Client{}

	result := CreateJobResponse{}

	req, err := z.request(POST, "jobs", setting)
	if err != nil {
		return &result, err
	}

	resp, err := client.Do(req.WithContext(ctx))
	if err != nil {
		return &result, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return &result, err
	}

	if resp.StatusCode != http.StatusCreated {
		var errorResponse ErrorResponse
		err := json.Unmarshal(body, &errorResponse)
		if err != nil {
			return &result, fmt.Errorf("There was a problem with the encoding request - %v", err.Error())
		}
		return &result, fmt.Errorf("There was a problem with the encoding request - %v", strings.Join(errorResponse.Errors, ","))
	}

	if err := json.Unmarshal(body, &result); err != nil {
		return &result, err
	}

	return &result, nil
}
