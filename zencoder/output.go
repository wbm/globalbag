package zencoder

import "time"

// OutputSettings are basically the general output settings of any encoding job.
// ZenDoc: https://app.zencoder.com/docs/api/encoding/general-output-settings
type OutputSettings struct {
	Type                string                        `json:"type,omitempty"`
	Label               string                        `json:"label,omitempty"`
	URL                 string                        `json:"url,omitempty"`
	SecondaryURL        string                        `json:"secondary_url,omitempty"`
	BaseURL             string                        `json:"base_url,omitempty"`
	Filename            string                        `json:"filename,omitempty"`
	PackageFilename     string                        `json:"package_filename,omitempty"`
	PackageFormat       string                        `json:"package_format,omitempty"`
	DeviceProfile       string                        `json:"device_profile,omitempty"`
	Strict              bool                          `json:"strict,omitempty"`
	SkipVideo           bool                          `json:"skip_video,omitempty"`
	SkipAudio           bool                          `json:"skip_audio,omitempty"`
	Source              string                        `json:"source,omitempty"`
	Credentials         string                        `json:"credentials,omitempty"`
	GenerateMD5Checksum bool                          `json:"generate_md5_checksum,omitempty"`
	ParallelUploadLimit int                           `json:"parallel_upload_limit,omitempty"`
	Headers             map[string]string             `json:"headers,omitempty"`
	Thumbnails          []*Thumbnail                  `json:"thumbnails,omitempty"`
	Watermarks          []*Watermark                  `json:"watermarks,omitempty"`
	Notifications       []*NotificationSettings       `json:"notifications,omitempty"`
	AlternateAudio      map[string][]CustomAudioGroup `json:"alternate_audio,omitempty"`
	//FLV           []*CuePoint             `json:"cue_points,omitempty"` // FLV o_O - https://blogs.adobe.com/conversations/2017/07/adobe-flash-update.html

	FormatAndCodec
	Resolution
	RateControl
	FrameRate
	Audio
	// Caption
	// LiveStreaming
	VideoProcessing
	AudioProcessing
	Clip
	// S3Settings
	// ConditionalOuputs
	SegmentedStreaming
	// Encryption
	// Decryption
	// H264
	// VP6
	// MP4
	// AAC
	// Aspera
	Transmuxing
}

type CustomAudioGroup struct {
	Name     string `json:"name,omitempty"`
	Source   string `json:"source,omitempty"`
	Path     string `json:"path,omitempty"`
	Language string `json:"language,omitempty"`
	Default  bool   `json:"default,omitempty"`
}

// FormatAndCodec ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/format-and-codecs
type FormatAndCodec struct {
	Format     string `json:"format,omitempty"`
	VideoCodec string `json:"video_codec,omitempty"`
	AudioCodec string `json:"audio_codec,omitempty"`
}

// Resolution settings for your encoding job.
// ZenDoc: https://app.zencoder.com/docs/api/encoding/resolution
type Resolution struct {
	Size       string `json:"size,omitempty"`
	Width      int    `json:"width,omitempty"`
	Height     int    `json:"height,omitempty"`
	Upscale    bool   `json:"upscale,omitempty"`
	AspectMode string `json:"aspect_mode,omitempty"`
}

// RateControl settings for your encode job
// ZenDoc: https://app.zencoder.com/docs/api/encoding/rate-control
type RateControl struct {
	Quality              int  `json:"quality,omitempty"`
	VideoBitrate         int  `json:"video_bitrate,omitempty"`
	AudioQuality         int  `json:"audio_quality,omitempty"`
	AudioBitrate         int  `json:"audio_bitrate,omitempty"`
	MaxVideoBitrate      int  `json:"max_video_bitrate,omitempty"`
	Speed                int  `json:"speed,omitempty"`
	DecoderBitrateCap    int  `json:"decoder_bitrate_cap,omitempty"`
	DecoderBufferSize    int  `json:"decoder_buffer_size,omitempty"`
	OnePass              bool `json:"one_pass,omitempty"`
	AudioConstantBitrate bool `json:"audio_constant_bitrate,omitempty"`
}

// FrameRate settings for your encode job
// ZenDoc: https://app.zencoder.com/docs/api/encoding/frame-rate
type FrameRate struct {
	FrameRate              int     `json:"frame_rate,omitempty"`
	MaxFrameRate           int     `json:"max_frame_rate,omitempty"`
	Decimate               int     `json:"decimate,omitempty"`
	KeyframeInterval       int     `json:"keyframe_interval,omitempty"`
	KeyframeRate           int     `json:"keyframe_rate,omitempty"`
	FixedKeyframeInterval  bool    `json:"fixed_keyframe_interval,omitempty"`
	ForcedKeyframeInterval int     `json:"forced_keyframe_interval,omitempty"`
	ForcedKeyframeRate     float64 `json:"forced_keyframe_rate,omitempty"`
}

// Video ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/video
type Video struct {
	VideoReferenceFrames string
}

// Audio ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/audio
type Audio struct {
	AudioSampleRate    int `json:"audio_sample_rate,omitempty"`
	MaxAudioSampleRate int `json:"max_audio_sample_rate,omitempty"`
	AudioChannels      int `json:"audio_channels,omitempty"`
}

// Thumbnail are settings that is needed to describe thumbnail controls for zencoder.
// ZenDoc: https://app.zencoder.com/docs/api/encoding/thumbnails
type Thumbnail struct {
	Label             string `json:"label,omitempty"`
	Format            string `json:"format,omitempty"`
	Number            int    `json:"number,omitempty"`
	StartAtFirstFrame bool   `json:"start_at_first_frame,omitempty"`
	Interval          int    `json:"interval,omitempty"`
	IntervalInFrames  int    `json:"interval_in_frames,omitempty"`
	Times             []int  `json:"times,omitempty"`
	AspectMode        string `json:"aspect_mode,omitempty"`
	Size              string `json:"size,omitempty"`
	Width             int    `json:"width,omitempty"`
	Height            int    `json:"height,omitempty"`
	BaseURL           string `json:"base_url,omitempty"`
	Prefix            string `json:"prefix,omitempty"`
	Filename          string `json:"filename,omitempty"`
	MakePublic        bool   `json:"public,omitempty"`
	// AccessControl       []*AccessControl  `json:"access_control,omitempty"`
	UseRRS              bool              `json:"rrs,omitempty"`
	Headers             map[string]string `json:"headers,omitempty"`
	Credentials         string            `json:"credentials,omitempty"`
	ParallelUploadLimit int               `json:"parallel_upload_limit,omitempty"`
}

// Watermark are settings related to watermarking your video outputs.
// ZenDoc: https://app.zencoder.com/docs/api/encoding/watermarks
type Watermark struct {
	URL     string  `json:"url,omitempty"`
	X       int     `json:"x,omitempty"`
	Y       int     `json:"y,omitempty"`
	Width   int     `json:"width,omitempty"`
	Height  int     `json:"height,omitempty"`
	Origin  string  `json:"origin,omitempty"`
	Opacity float64 `json:"opacity,omitempty"`
}

// Caption is in BETA with zencoder
// ZenDoc: https://app.zencoder.com/docs/api/encoding/captions
// type Caption struct {
// 	CaptionURL   string `json:"caption_url,omitempty"`
// 	SkipCaptions bool   `json:"skip_captions,omitempty"`
// }

// LiveStreaming ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/live-streaming
// type LiveStreaming struct {
// 	LiveStream                bool `json:"live_stream,omitempty"`
// 	ReconnectTime             int  `json:"reconnect_time,omitempty"`
// 	EventLength               int  `json:"event_length,omitempty"`
// 	LiveSlidingWindowDuration int  `json:"live_sliding_window_duration,omitempty"`
// }

// VideoProcessing ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/video-processing
type VideoProcessing struct {
	Rotate      int    `json:"rotate,omitempty"`
	Deinterlace string `json:"deinterlace,omitempty"`
	Sharpen     bool   `json:"sharpen,omitempty"`
	Denoise     string `json:"denoise,omitempty"`
	Autolevel   bool   `json:"autolevel,omitempty"`
	Deblock     bool   `json:"deblock,omitempty"`
}

// AudioProcessing ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/audio-processing
type AudioProcessing struct {
	AudioGain                 int     `json:"audio_gain,omitempty"`
	AudioNormalize            bool    `json:"audio_normalize,omitempty"`
	AudioPreNormalize         bool    `json:"audio_pre_normalize,omitempty"`
	AudioPostNormalize        bool    `json:"audio_post_normalize,omitempty"`
	AudioBass                 int     `json:"audio_bass,omitempty"`
	AudioTreble               int     `json:"audio_treble,omitempty"`
	AudioHighpass             int     `json:"audio_highpass,omitempty"`
	AudioLowpass              int     `json:"audio_lowpass,omitempty"`
	AudioCompressionRatio     float64 `json:"audio_compression_ratio,omitempty"`
	AudioCompressionThreshold int     `json:"audio_compression_threshold,omitempty"`
	AudioExpansionRatio       float64 `json:"audio_expansion_ratio,omitempty"`
	AudioExpansionThreshold   int     `json:"audio_expansion_threshold,omitempty"`
	AudioFade                 float64 `json:"audio_fade,omitempty"`
	AudioFadeIn               float64 `json:"audio_fade_in,omitempty"`
	AudioFadeOut              float64 `json:"audio_fade_out,omitempty"`
	AudioKaraokeMode          bool    `json:"audio_karaoke_mode,omitempty"`
}

// Clip ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/clips
type Clip struct {
	StartClip  string `json:"start_clip,omitempty"`
	ClipLength string `json:"clip_length,omitempty"`
}

// S3Settings ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/s3-settings
// type S3Settings struct {
// 	MakePublic    bool             `json:"public,omitempty"`
// 	UseRRS        bool             `json:"rrs,omitempty"`
// 	AccessControl []*AccessControl `json:"access_control,omitempty"`
// }

// NotificationSettings are settings related to what notifications need to happen when
// the encoding process is completed.
// ZenDoc: https://app.zencoder.com/docs/api/encoding/notifications
type NotificationSettings struct {
	URL     string            `json:"url,omitempty"`
	Format  string            `json:"format,omitempty"`
	Headers map[string]string `json:"headers,omitempty"`
	Event   string            `json:"event,omitempty"`
}

// ConditionalOuputs is probably deprecated
// ZenDoc: https://app.zencoder.com/docs/api/encoding/conditional-outputs
type ConditionalOuputs struct {
	MinSize         string `json:"min_size,omitempty"`
	MaxSize         string `json:"max_size,omitempty"`
	MinDuration     int    `json:"min_duration,omitempty"`
	MaxDuration     int    `json:"max_duration,omitempty"`
	MinAudioBitRate int    `json:"min_audio_bitrate"`
	MaxAudioBitRate int    `json:"max_audio_bitrate"`
	MinVideoBitRate int    `json:"min_video_bitrate"`
	MaxVideoBitRate int    `json:"max_video_bitrate"`
	RequireAudio    bool   `json:"require_audio"`
	RequireVideo    bool   `json:"require_video"`
}

// SegmentedStreaming ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/segmented-streaming
type SegmentedStreaming struct {
	SegmentSeconds        int               `json:"segment_seconds,omitempty"`
	SegmentSize           int               `json:"segment_size,omitempty"`
	Streams               []*StreamSettings `json:"streams,omitempty"`
	SegmentImageURL       string            `json:"segment_image_url,omitempty"`
	SegmentVideoSnapshots bool              `json:"segment_video_snapshots,omitempty"`
	MaxHLSProtocolVersion int               `json:"max_hls_protocol_version,omitempty"`
	HLSOptimizedTS        bool              `json:"hls_optimized_ts,omitempty"`
	PrepareForSegmenting  string            `json:"prepare_for_segmenting,omitempty"`
	InstantPlay           bool              `json:"instant_play,omitempty"`
	SMILBaseURL           string            `json:"smil_base_url,omitempty"`
}

// Encryption ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/encryption
// type Encryption struct {
// 	EncryptionMethod            string `json:"encryption_method,omitempty"`
// 	EncryptionKey               string `json:"encryption_key,omitempty"`
// 	EncryptionKeyURL            string `json:"encryption_key_url,omitempty"`
// 	EncryptionKeyRotationPeriod int    `json:"encryption_key_rotation_period,omitempty"`
// 	EncryptionKeyURLPrefix      string `json:"encryption_key_url_prefix,omitempty"`
// 	EncryptionIV                string `json:"encryption_iv,omitempty"`
// 	EncryptionPassword          string `json:"encryption_password,omitempty"`
// }

// Decryption ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/decryption
// type Decryption struct {
// 	DecryptionMethod   string `json:"decryption_method,omitempty"`
// 	DecryptionKey      string `json:"decryption_key,omitempty"`
// 	DecryptionKeyURL   string `json:"decryption_key_url,omitempty"`
// 	DecryptionPassword string `json:"decryption_password,omitempty"`
// }

// H264 ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/h264
type H264 struct {
	H264ReferenceFrames int    `json:"h264_reference_frames,omitempty"`
	H264Profile         string `json:"h264_profile,omitempty"`
	H264Level           string `json:"h264_level,omitempty"`
	H264BFrames         int    `json:"h264_bframes,omitempty"`
	Tuning              string `json:"tuning,omitempty"`
	CRF                 int    `json:"crf,omitempty"`
}

// FLV ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/flv
// type FLV struct {
// 	Type string            `json:"type,omitempty"`
// 	Time float64           `json:"time,omitempty"`
// 	Name string            `json:"name,omitempty"`
// 	Data map[string]string `json:"data,omitempty"`
// }

// VP6 ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/vp6
// type VP6 struct {
// 	VP6TemporalDownWatermark int     `json:"vp6_temporal_down_watermark,omitempty"`
// 	VP6TemporalResampling    bool    `json:"vp6_temporal_resampling,omitempty"`
// 	VP6UndershootPct         int     `json:"vp6_undershoot_pct,omitempty"`
// 	VP6Profile               string  `json:"vp6_profile,omitempty"`
// 	VP6CompressionMode       string  `json:"vp6_compression_mode,omitempty"`
// 	VP6TwoPassMinSection     int     `json:"vp6_2pass_min_section,omitempty"`
// 	VP6TwoPassMaxSection     int     `json:"vp6_2pass_max_section,omitempty"`
// 	VP6StreamPrebuffer       int     `json:"vp6_stream_prebuffer,omitempty"`
// 	VP6StreamMaxBuffer       int     `json:"vp6_stream_max_buffer,omitempty"`
// 	VP6DeinterlaceMode       string  `json:"vp6_deinterlace_mode,omitempty"`
// 	VP6DenoiseLevel          float64 `json:"vp6_denoise_level,omitempty"`
// 	AlphaTransparency        bool    `json:"alpha_transparency,omitempty"`
// 	ConstantBitrate          bool    `json:"constant_bitrate,omitempty"`
// }

// MP4 ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/mp4
// type MP4 struct {
// 	Hint    bool `json:"hint,omitempty"`
// 	MTUSize int  `json:"mtu_size,omitempty"`
// }

// AAC ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/aac
// type AAC struct {
// 	MaxAacProfile   string `json:"max_aac_profile,omitempty"`   // What is the most advanced (compressed) AAC profile to allow?
// 	ForceAacProfile string `json:"force_aac_profile,omitempty"` // Force the use of a particular AAC profile, rather than letting Zencoder choose the best profile for the bitrate.
// }

// Aspera ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/aspera
// type Aspera struct {
// 	AsperaTransferPolicy string `json:"aspera_transfer_policy,omitempty"`
// 	TransferMinimumRate  int    `json:"transfer_minimum_rate,omitempty"`
// 	TransferMaximumRate  int    `json:"transfer_maximum_rate,omitempty"`
// }

// Transmuxing ...
// ZenDoc: https://app.zencoder.com/docs/api/encoding/transmuxing
type Transmuxing struct {
	CopyVideo bool `json:"copy_video,omitempty"`
	CopyAudio bool `json:"copy_audio,omitempty"`
}

// AccessControl is a setting that can be set on (outputs, thumbnails, etc.) that has a grantee and a permission.
// ZenDoc: https://app.zencoder.com/docs/api/encoding/thumbnails/thumbnails-access-control
// Related: https://app.zencoder.com/docs/api/encoding/s3-settings/access-control
// type AccessControl struct {
// 	Grantee    string   `json:"grantee,omitempty"`
// 	Permission []string `json:"permission,omitempty"`
// }

// StreamSettings is part of SegmentedStreaming ¯\_(ツ)_/¯
// ZenDoc: https://app.zencoder.com/docs/api/encoding/segmented-streaming
type StreamSettings struct {
	Path       string `json:"path,omitempty"`
	Bandwidth  int    `json:"bandwidth,omitempty"`
	Resolution string `json:"resolution,omitempty"`
	Codecs     string `json:"codecs,omitempty"`
	Source     string `json:"source,omitempty"`
	Audio      string `json:"audio,omitempty"`
	Default    bool   `json:"default,omitempty"`
}

type Notification struct {
	Job struct {
		ID          int       `json:"id"`
		State       string    `json:"state"`
		CreatedAt   time.Time `json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at"`
		SubmittedAt time.Time `json:"submitted_at"`
		PassThrough bool      `json:"pass_through"`
		Test        bool      `json:"test"`
	} `json:"job"`
	Input struct {
		ID                 int     `json:"id"`
		Format             string  `json:"format"`
		FrameRate          float64 `json:"frame_rate"`
		DurationInMs       int     `json:"duration_in_ms"`
		AudioSampleRate    int     `json:"audio_sample_rate"`
		AudioBitrateInKbps int     `json:"audio_bitrate_in_kbps"`
		AudioCodec         string  `json:"audio_codec"`
		Height             int     `json:"height"`
		Width              int     `json:"width"`
		FileSizeInBytes    int     `json:"file_size_in_bytes"`
		VideoCodec         string  `json:"video_codec"`
		TotalBitrateInKbps int     `json:"total_bitrate_in_kbps"`
		Channels           string     `json:"channels"`
		VideoBitrateInKbps int     `json:"video_bitrate_in_kbps"`
		State              string  `json:"state"`
		Md5Checksum        string  `json:"md5_checksum"`
	} `json:"input"`
	Output struct {
		ID                   int     `json:"id"`
		URL                  string  `json:"url"`
		Label                string  `json:"label"`
		State                string  `json:"state"`
		Format               string  `json:"format"`
		Type                 string  `json:"type"`
		FrameRate            float64 `json:"frame_rate"`
		DurationInMs         int     `json:"duration_in_ms"`
		AudioSampleRate      int     `json:"audio_sample_rate"`
		AudioBitrateInKbps   int     `json:"audio_bitrate_in_kbps"`
		AudioCodec           string  `json:"audio_codec"`
		Height               int     `json:"height"`
		Width                int     `json:"width"`
		FileSizeInBytes      int     `json:"file_size_in_bytes"`
		VideoCodec           string  `json:"video_codec"`
		TotalBitrateInKbps   int     `json:"total_bitrate_in_kbps"`
		Channels             string     `json:"channels"`
		VideoBitrateInKbps   int     `json:"video_bitrate_in_kbps"`
		FragmentDurationInMs int     `json:"fragment_duration_in_ms"`
		Rfc6381VideoCodec    string  `json:"rfc_6381_video_codec"`
		Rfc6381AudioCodec    string  `json:"rfc_6381_audio_codec"`
		Thumbnails           []struct {
			Label  string `json:"label"`
			Images []struct {
				Dimensions    string `json:"dimensions"`
				FileSizeBytes int    `json:"file_size_bytes"`
				Format        string `json:"format"`
				URL           string `json:"url"`
			} `json:"images"`
		} `json:"thumbnails"`
		Md5Checksum string `json:"md5_checksum"`
	} `json:"output"`
}
