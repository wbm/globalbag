package zencoder

import (
	"fmt"
)

const (
	apiServer      = "https://app.zencoder.com/api"
	apiVersion     = "v2"
	apiHeader      = "Zencoder-Api-Key"
	apiContentType = "application/json"
)

// Client holds all the default settings that we'll need to interface with zencoder.
type Client struct {
	server      string
	apikey      string
	contentType string
}

// New returns a ZencoderClient that allows the user to interact with the zencoder api.
func New(apiKey string) *Client {
	return &Client{
		server:      fmt.Sprintf("%v/%v", apiServer, apiVersion),
		apikey:      apiKey,
		contentType: "application/json",
	}
}

var (
	// GET is a common verb for getting data via new request.
	GET = "GET"
	// POST is a common verb for posting data via new request.
	POST = "POST"
)
