package main

import (
	"fmt"

	"github.com/webRat/zencoder"
)

func main() {
	zclient := zencoder.New("829d361242f222102f2077d29a1fa63d")

	settings := zencoder.JobSetting{
		Input: "https://mpc-zencoder-test.s3.amazonaws.com/spinner.mp4",
		Test:  false,
		Output: []*zencoder.OutputSettings{
			{
				Thumbnails: []*zencoder.Thumbnail{
					{
						Credentials:         "s3Zencoder",
						ParallelUploadLimit: 10,
						Format:              "jpg",
						Label:               "Largest",
						Width:               1280,
						Height:              720,
						Interval:            1,
						StartAtFirstFrame:   true,
						BaseURL:             "s3://mpc-zencoder-test/thumbnails/test/",
						Filename:            "{{width}}x{{height}}-{{number}}",
					},
					{
						Credentials:         "s3Zencoder",
						ParallelUploadLimit: 10,
						Format:              "jpg",
						Label:               "Smaller",
						Width:               640,
						Height:              360,
						Interval:            1,
						StartAtFirstFrame:   true,
						BaseURL:             "s3://mpc-zencoder-test/thumbnails/test/",
						Filename:            "{{width}}x{{height}}-{{number}}",
					},
				},
				Label: "hd",
				Resolution: zencoder.Resolution{
					Height: 720,
					Width:  1280,
				},
				FormatAndCodec: zencoder.FormatAndCodec{
					VideoCodec: "h264",
				},
				Audio: zencoder.Audio{
					AudioSampleRate: 44100,
					AudioChannels:   2,
				},
				FrameRate: zencoder.FrameRate{
					KeyframeInterval: 48,
				},
				RateControl: zencoder.RateControl{
					AudioBitrate:    128,
					VideoBitrate:    2000,
					MaxVideoBitrate: 2500,
				},
				VideoProcessing: zencoder.VideoProcessing{
					Deinterlace: "detect",
				},
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/hd.mp4",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				Label: "high",
				Resolution: zencoder.Resolution{
					Height: 540,
					Width:  960,
				},
				FormatAndCodec: zencoder.FormatAndCodec{
					VideoCodec: "h264",
				},
				Audio: zencoder.Audio{
					AudioSampleRate: 44100,
					AudioChannels:   2,
				},
				FrameRate: zencoder.FrameRate{
					KeyframeInterval: 48,
				},
				RateControl: zencoder.RateControl{
					AudioBitrate:    96,
					VideoBitrate:    1200,
					MaxVideoBitrate: 1600,
				},
				VideoProcessing: zencoder.VideoProcessing{
					Deinterlace: "detect",
				},
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/high.mp4",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				Label: "med",
				Resolution: zencoder.Resolution{
					Height: 480,
					Width:  854,
				},
				FormatAndCodec: zencoder.FormatAndCodec{
					VideoCodec: "h264",
				},
				Audio: zencoder.Audio{
					AudioSampleRate: 44100,
					AudioChannels:   1,
				},
				FrameRate: zencoder.FrameRate{
					KeyframeInterval: 48,
				},
				RateControl: zencoder.RateControl{
					AudioBitrate:    64,
					VideoBitrate:    800,
					MaxVideoBitrate: 1200,
					Speed:           1,
				},
				VideoProcessing: zencoder.VideoProcessing{
					Deinterlace: "detect",
				},
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/med.mp4",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				Label: "low",
				Resolution: zencoder.Resolution{
					Height: 360,
					Width:  640,
				},
				FormatAndCodec: zencoder.FormatAndCodec{
					VideoCodec: "h264",
				},
				Audio: zencoder.Audio{
					AudioSampleRate: 32000,
					AudioChannels:   1,
				},
				FrameRate: zencoder.FrameRate{
					KeyframeInterval: 48,
				},
				RateControl: zencoder.RateControl{
					AudioBitrate:    48,
					VideoBitrate:    400,
					MaxVideoBitrate: 500,
					Speed:           2,
				},
				VideoProcessing: zencoder.VideoProcessing{
					Deinterlace: "detect",
				},
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/low.mp4",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				FormatAndCodec: zencoder.FormatAndCodec{
					Format: "ts",
				},
				Transmuxing: zencoder.Transmuxing{
					CopyAudio: true,
					CopyVideo: true,
				},
				Source:              "hd",
				Label:               "hls-hd",
				Type:                "segmented",
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/hls/hd.m3u8",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				FormatAndCodec: zencoder.FormatAndCodec{
					Format: "ts",
				},
				Transmuxing: zencoder.Transmuxing{
					CopyAudio: true,
					CopyVideo: true,
				},
				Source:              "high",
				Label:               "hls-high",
				Type:                "segmented",
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/hls/high.m3u8",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				FormatAndCodec: zencoder.FormatAndCodec{
					Format: "ts",
				},
				Transmuxing: zencoder.Transmuxing{
					CopyAudio: true,
					CopyVideo: true,
				},
				Source:              "med",
				Label:               "hls-med",
				Type:                "segmented",
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/hls/med.m3u8",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				FormatAndCodec: zencoder.FormatAndCodec{
					Format: "ts",
				},
				Transmuxing: zencoder.Transmuxing{
					CopyAudio: true,
					CopyVideo: true,
				},
				Source:              "low",
				Label:               "hls-low",
				Type:                "segmented",
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/hls/low.m3u8",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
			{
				SegmentedStreaming: zencoder.SegmentedStreaming{
					Streams: []*zencoder.StreamSettings{
						{
							Path:      "hls/hd.m3u8",
							Bandwidth: 2000,
						},
						{
							Path:      "hls/high.m3u8",
							Bandwidth: 1200,
						},
						{
							Path:      "hls/med.m3u8",
							Bandwidth: 800,
						},
						{
							Path:      "hls/low.m3u8",
							Bandwidth: 400,
						},
					},
				},
				Type:                "playlist",
				URL:                 "ftp://adobetv.upload.akamai.com/77611/adobetv2/_images/mpcv3/test/hls/playlist.m3u8",
				Credentials:         "akamai-5",
				ParallelUploadLimit: 2,
			},
		},
	}

	// b, err := json.Marshal(settings)
	// if err != nil {
	// 	panic(err)
	// }
	// fmt.Printf("+%v", string(b))

	job, err := zclient.CreateJob(settings)
	fmt.Println(job, err)

}
