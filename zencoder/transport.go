package zencoder

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
)

func (z *Client) request(verb, resource string, request interface{}) (*http.Request, error) {
	var buffer io.Reader

	if request != nil {
		j, err := json.Marshal(request)
		if err != nil {
			return nil, err
		}
		buffer = bytes.NewBuffer(j)
	}

	req, err := http.NewRequest(verb, fmt.Sprintf("%v/%v", z.server, resource), buffer)
	if err == nil {
		// If there's no errors, go ahead and add the needed headers
		req.Header.Add("Zencoder-Api-Key", z.apikey)
		req.Header.Add("Content-Type", z.contentType)
	}

	return req, err
}
