package creativecloud

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

const (
	production = "cc-collab.adobe.io"
	staging    = "cc-collab-stage.adobe.io"
)

// GetProfile requires a susi token and returns the user's creative cloud profile
func GetProfile(env, token string) (ProfileResponse, error) {
	var response ProfileResponse

	var domain string
	switch strings.ToLower(env) {
	case "production":
		domain = production
	default:
		domain = staging
	}

	client := http.Client{}
	endpoint := fmt.Sprintf("https://%v/profile", domain)
	req, err := http.NewRequest(http.MethodGet, endpoint, nil)
	if err != nil {
		return response, err
	}

	if token == "" || !strings.Contains(token, "Bearer ") {
		return response, errors.WithStack(errors.New("invalid SUSI token"))
	}
	req.Header.Set("Authorization", token)

	res, err := client.Do(req)
	if err != nil {
		return response, err
	}

	defer res.Body.Close()

	bodyData, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return response, err
	}

	err = json.Unmarshal(bodyData, &response)
	if err != nil {
		return response, err
	}

	return response, nil
}

type ProfileResponse struct {
	User ProfileUser `json:"user"`
}

type ProfileUser struct {
	Avatar string `json:"avatar"`
}
