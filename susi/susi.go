package susi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
)

const (
	production = "ims-na1.adobelogin.com"
	staging    = "ims-na1-stg1.adobelogin.com"
)

var (
	// Err401 is an unathorized string from SUSI
	Err401 = errors.New("401. Unauthorized")
	// ErrUnknownDomain is an error that will be thrown if passed an unknown domain.
	ErrUnknownDomain = errors.New("Unknown SUSI domain")
	// ErrInvalidToken is an error that will be thrown if passed an invalid token.
	ErrInvalidToken = errors.New("invalid SUSI token")
	// ErrInvalidClientID is an error that will be thrown if passed an invalid client id.
	ErrInvalidClientID = errors.New("invalid client id")
)

// Response ...
type Response struct {
	AdobeID   string `json:"userId"`
	Email     string `json:"email"`
	Name      string `json:"name"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	UTC       string `json:"utcOffset"`
	// DON'T CARE ABOUT
	// PhoneNumber string "phoneNumber": null,
	// Languages? "preferred_languages": null,
	AccountType              string `json:"account_type"`
	Address                  string `json:"address"`
	CountryCode              string `json:"countryCode"`
	DisplayName              string `json:"displayName"`
	EmailVerified            string `json:"emailVerified"`
	MarketingPermissions     string `json:"mrktPerm"`
	MarketingPermissionEmail string `json:"mrktPermEmail"`
}

// NewRequest ...
func NewRequest(env, clientID string) (*http.Request, error) {
	var susiDomain string
	switch strings.ToLower(env) {
	case "production":
		susiDomain = production
	default:
		susiDomain = staging
	}

	if clientID == "" {
		return &http.Request{}, errors.WithStack(ErrInvalidClientID)
	}

	url := fmt.Sprintf("https://%v//ims/profile/v1?client_id=%v", susiDomain, clientID)
	return http.NewRequest("GET", url, nil)
}

// ValidateToken will accept a domain and a susi token so that we can validate
// the token against Adobe SUSI
func ValidateToken(client *http.Client, req *http.Request, token string) (Response, error) {
	var response Response
	if token == "" || !strings.Contains(token, "Bearer ") {
		return response, errors.WithStack(ErrInvalidToken)
	}

	req.Header.Set("Authorization", token)

	resp, err := client.Do(req)
	if err != nil {
		return response, errors.WithStack(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode == http.StatusUnauthorized {
		return response, Err401
	}

	if err := json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return response, errors.WithStack(err)
	}

	return response, nil
}
